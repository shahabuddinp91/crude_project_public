<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Mobile\Mobile;
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//session_destroy();
$Mobile =new Mobile();
$allmobiles = $Mobile->trashted();
?>
<html>
    <a href="create.php">Create</a>
    <a href="index.php">See All Data</a>
    <br>
    <table align="center" border="1">
        <tr>
            <th>SL</th>
            <th>Mobile Model</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($allmobiles) && !empty($allmobiles)){
            $serial=0;
            foreach ($allmobiles as $oneMobile){
                $serial++
                        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $oneMobile['title']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $oneMobile['id'] ?>">Show Details</a>  |
                  <a href="edit.php?id=<?php echo $oneMobile['id'] ?>">Edit</a>  |
                  <a href="delete.php?id=<?php echo $oneMobile['id'] ?>">Delete Permanently</a>  |
            </td>
        </tr>
        <?php
            }
        }
        else{
            ?>
        <tr>
            <td colspan="3">
                <?php   echo"Not available";?>
            </td>
        </tr>
        <?php
        }
        ?>
        
    </table>
</html>