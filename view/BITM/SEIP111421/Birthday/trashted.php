<?php
session_start();
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Birthday\Birthday;
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//session_destroy();
$birthday =new Birthday();
$allbirthday = $birthday->trashed();
?>
<html>
    <a href="create.php">Create</a>
    <a href="index.php">See All Data</a>
    <br>
    <table align="center" border="1">
        <tr>
            <th>SL</th>
            <th>Birthday</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($allbirthday) && !empty($allbirthday)){
            $serial=0;
            foreach ($allbirthday as $onebirthday){
                $serial++
                        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $onebirthday['birthday'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $onebirthday['id'] ?>">Show Details</a>  |
                  <a href="edit.php?id=<?php echo $onebirthday['id'] ?>">Edit</a>  |
                  <a href="delete.php?id=<?php echo $onebirthday['id'] ?>">Delete Permanently</a>  |
            </td>
        </tr>
        <?php
            }
        }
        else{
            ?>
        <tr>
            <td colspan="3">
                <?php   echo"Not available";?>
            </td>
        </tr>
        <?php
        }
        ?>
        
    </table>
</html>