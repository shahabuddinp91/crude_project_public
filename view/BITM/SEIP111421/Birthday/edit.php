<?php
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Birthday\Birthday;
$id=$_GET['id'];
$birthday = new Birthday();
$onebirthday=$birthday->show($id);

?>

<html>
    <head>
        <title>Update | Birthday</title>
    </head>
    <body>
        <fieldset>
            <legend>
                Crud of Birthday
            </legend> 
            <form action="update.php" method="POST">
                <label>Enter Your Birthday</label><br/>
                <input type="date" name="title" id="title" value="<?php echo $onebirthday['birthday'];?>"><br/>
                <input type="submit" value="Update">
                <!--<input type="submit" value="Save & Enter Again">-->
                <input type="hidden" name="id" value="<?php echo $id ?>">
            </form>
        </fieldset>
    </body>
</html>