<?php
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

?>

<html>
    <head>
        <title>Create | Birthday</title>
    </head>
    <body>
        <a href="index.php">See All Data</a>
        <fieldset>
            <legend>
                Crud of Birthday
            </legend> 
            <form action="store.php" method="POST">
                <label>Enter Your Birthday</label><br/>
                <input type="date" name="title" id="title" placeholder="Y-M-D"><br/>
                <input type="submit" value="Save">
                <!--<input type="submit" value="Save & Enter Again">-->
                <input type="reset" value="Reset">
            </form>
        </fieldset>
    </body>
</html>