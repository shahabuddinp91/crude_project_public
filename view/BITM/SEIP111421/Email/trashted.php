<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Email\Email;
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//session_destroy();
$email =new Email();
$allemails = $email->trashed();
?>
<html>
    <a href="create.php">Create</a>
    <a href="index.php">See All Data</a>
    <br>
    <table align="center" border="1">
        <tr>
            <th>SL</th>
            <th>Email Address</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($allemails) && !empty($allemails)){
            $serial=0;
            foreach ($allemails as $oneemail){
                $serial++
                        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $oneemail['email']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $oneemail['id'] ?>">Show Details</a>  |
                  <a href="edit.php?id=<?php echo $oneemail['id'] ?>">Edit</a>  |
                  <a href="delete.php?id=<?php echo $oneemail['id'] ?>">Delete Permanently</a>  |
            </td>
        </tr>
        <?php
            }
        }
        else{
            ?>
        <tr>
            <td colspan="3">
                <?php   echo"Not available";?>
            </td>
        </tr>
        <?php
        }
        ?>
        
    </table>
</html>