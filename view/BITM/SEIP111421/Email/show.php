<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Email\Email;
$id=$_GET['id'];
$email =new Email();
$oneemail=$email->show($id);
?>
<html>
    <table border="1" align="center">
        <tr>
            <th>ID</th>
            <th>Email Address</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $oneemail['id']?></td>
            <td>
                <?php 
                if(isset($oneemail['email']) && !empty($oneemail['email'])){
                    echo $oneemail['email'];
                }else{
                    echo "Not Available";
                }
                ?>
            </td>
            <td><?php echo $oneemail['created']?></td>
            <td>
                <a href="edit.php?id=<?php echo $oneemail['id'];?>">Edit</a> | 
                <a href="delete.php?id=<?php echo $oneemail['id'];?>">Delete</a> 
            </td>
        </tr>
    </table>
</html>
