<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Gender\Gender;
$id=$_GET['id'];
$gender =new Gender();
$onegender=$gender->show($id);
?>
<html>
    <table border="1" align="center">
        <tr>
            <th>ID</th>
            <th>Gender</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $onegender['id']?></td>
            <td>
                <?php 
                if(isset($onegender['gender']) && !empty($onegender['gender'])){
                    echo $onegender['gender'];
                }else{
                    echo "Not Available";
                }
                ?>
            </td>
            <td><?php echo $onegender['created']?></td>
            <td>
                <a href="edit.php?id=<?php echo $onegender['id'];?>">Edit</a> | 
                <a href="delete.php?id=<?php echo $onegender['id'];?>">Delete</a> 
            </td>
        </tr>
    </table>
</html>
