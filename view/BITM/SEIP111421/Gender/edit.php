<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Gender\Gender;
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$id=$_GET['id'];
$gender = new Gender();
$onegender=$gender->show($id);
?>
<html>
    <fieldset>
        <legend>Gender Form</legend>
        <table align="center" border="0">
            <form action="update.php" method="post">
               <tr>
                    <td>Gender</td>
                    <td>:</td>
                    <td>
                        <input type="radio" name="gender" value="Male"
                               <?php 
                               if(preg_match("/Male/", $onegender['gender'])){
                                   echo 'Checked';
                               }
                               ?>
                               > Male |
                        <input type="radio" name="gender" value="Female"
                                 <?php 
                               if(preg_match("/Female/", $onegender['gender'])){
                                   echo 'Checked';
                               }
                               ?>
                               > Female
                    </td>
                </tr>
                <th colspan="2">
                <td>
                    <input type="submit" name="save" value="Update">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                </td>
                </th>
            
            </form>
            
        </table>
    </fieldset>
</html>
