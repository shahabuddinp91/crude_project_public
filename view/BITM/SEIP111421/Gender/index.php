<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Gender\Gender;
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$gender = new Gender();
$allgender = $gender->index();
?>
<html>
    <a href="create.php">Create Gender</a> | 
    <a href="trashted.php">All Deleted Item</a>
    <table border="1" align="center">
        <tr>
            <th>SL</th>
            <th>Gender</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($allgender) && !empty($allgender)){
            $serial=0;
            foreach($allgender as $onegender){
                $serial++
                        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $onegender['gender'];?></td>
            <td>
                <a href="show.php?id=<?php echo $onegender['id'];?>">Show Details</a> | 
                <a href="edit.php?id=<?php echo $onegender['id'];?>">Edit</a> | 
                <a href="trash.php?id=<?php echo $onegender['id'];?>">Delete</a>  
            </td>
        </tr>
        <?php 
        
            }
        }
 else {
     ?>
        <tr>
            <td colspan="3">
                <?php echo "Not Available" ?>
            </td>
        </tr>
 <?php
 
 }
        ?>
    </table>
</html>

