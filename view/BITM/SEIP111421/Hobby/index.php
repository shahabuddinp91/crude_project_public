<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Hobby\Hobbies;
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$hobby = new Hobbies();
$allhobbies = $hobby->index();
?>
<html>
    <a href="create.php">Create Book Title</a> | 
    <a href="trashted.php">All Deleted Item</a>
    <table border="1" align="center">
        <tr>
            <th>SL</th>
            <th>Hobby</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($allhobbies) && !empty($allhobbies)){
            $serial=0;
            foreach($allhobbies as $onehobby){
                $serial++
                        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $onehobby['hobby'];?></td>
            <td>
                <a href="show.php?id=<?php echo $onehobby['id'];?>">Show Details</a> | 
                <a href="edit.php?id=<?php echo $onehobby['id'];?>">Edit</a> | 
                <a href="trash.php?id=<?php echo $onehobby['id'];?>">Delete</a>  
            </td>
        </tr>
        <?php 
        
            }
        }
 else {
     ?>
        <tr>
            <td colspan="3">
                <?php echo "Not Available" ?>
            </td>
        </tr>
 <?php
 
 }
        ?>
    </table>
</html>