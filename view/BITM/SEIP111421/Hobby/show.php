<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Hobby\Hobbies;
$id=$_GET['id'];
$hobby =new Hobbies();
$onehobby=$hobby->show($id);
?>
<html>
    <table border="1" align="center">
        <tr>
            <th>ID</th>
            <th>Hobby</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $onehobby['id']?></td>
            <td>
                <?php 
                if(isset($onehobby['hobby']) && !empty($onehobby['hobby'])){
                    echo $onehobby['hobby'];
                }else{
                    echo "Not Available";
                }
                ?>
            </td>
            <td><?php echo $onehobby['created']?></td>
            <td>
                <a href="edit.php?id=<?php echo $onehobby['id'];?>">Edit</a> | 
                <a href="delete.php?id=<?php echo $onehobby['id'];?>">Delete</a> 
            </td>
        </tr>
    </table>
</html>
