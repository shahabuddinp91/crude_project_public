<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Hobby\Hobbies;
$hobby = new Hobbies();
$hobby->prepare($_GET);
$oneHobby = $hobby->edit();

?>
<html>
    <body>
        <fieldset>
            <legend>Select Your Favourite Hobbies</legend>
            <form action="update.php" method="post" >
                <input type="hidden" name="id" value="<?php echo $oneHobby['id']; ?>">
                <input type="checkbox" name="hobby[]" value="Cricket"
                    <?php if (preg_match("/Cricket/", $oneHobby['hobby'])) {
                       echo 'checked';
                   } else {
                       echo '';
                   } ?>
                       />Cricket<br>
                <input type="checkbox" name="hobby[]" value="Football"
                        <?php if (preg_match("/Football/", $oneHobby['hobby'])) {
                       echo 'checked';
                   } else {
                       echo '';
                   } ?>
                       >Football<br>
                <input type="checkbox" name="hobby[]" value="Codding"
                           <?php if (preg_match("/Codding/", $oneHobby['hobby'])) {
                       echo 'checked';
                   } else {
                       echo '';
                   } ?>
                       >Codding<br>
                <input type="checkbox" name="hobby[]" value="Gardening" 
                       <?php if (preg_match("/Gardening/", $oneHobby['hobby'])) {
                       echo 'checked';
                   } else {
                       echo '';
                   } ?>
                       >Gardening<br>
                <input type="submit" value="Update" name="save">
            </form>
        </fieldset>
    </body>
</html>