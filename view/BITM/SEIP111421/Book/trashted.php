<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Book\book;
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
//session_destroy();
$Book =new book();
$allbooks = $Book->trashed();
?>
<html>
    <a href="create.php">Create</a>
    <a href="index.php">See All Data</a>
    <br>
    <table align="center" border="1">
        <tr>
            <th>SL</th>
            <th>Book Title</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($allbooks) && !empty($allbooks)){
            $serial=0;
            foreach ($allbooks as $onebook){
                $serial++
                        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $onebook['title']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $onebook['id'] ?>">Show Details</a>  |
                  <a href="edit.php?id=<?php echo $onebook['id'] ?>">Edit</a>  |
                  <a href="delete.php?id=<?php echo $onebook['id'] ?>">Delete Permanently</a>  |
            </td>
        </tr>
        <?php
            }
        }
        else{
            ?>
        <tr>
            <td colspan="3">
                <?php   echo"Not available";?>
            </td>
        </tr>
        <?php
        }
        ?>
        
    </table>
</html>