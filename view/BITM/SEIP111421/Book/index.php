<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Book\book;
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$Books = new book();
$allbooks = $Books->index();
?>
<html>
    <a href="create.php">Create Book Title</a> | 
    <a href="trashted.php">All Deleted Item</a>
    <table border="1" align="center">
        <tr>
            <th>SL</th>
            <th>Book Title</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($allbooks) && !empty($allbooks)){
            $serial=0;
            foreach($allbooks as $onebook){
                $serial++
                        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $onebook['title'];?></td>
            <td>
                <a href="show.php?id=<?php echo $onebook['id'];?>">Show Details</a> | 
                <a href="edit.php?id=<?php echo $onebook['id'];?>">Edit</a> | 
                <a href="trash.php?id=<?php echo $onebook['id'];?>">Delete</a>  
            </td>
        </tr>
        <?php 
        
            }
        }
 else {
     ?>
        <tr>
            <td colspan="3">
                <?php echo "Not Available" ?>
            </td>
        </tr>
 <?php
 
 }
        ?>
    </table>
</html>
