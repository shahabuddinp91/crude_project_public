<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Book\book;
$id=$_GET['id'];
$Book =new book();
$onebook=$Book->show($id);
?>
<html>
    <table border="1" align="center">
        <tr>
            <th>ID</th>
            <th>Book Title</th>
            <th>Created</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $onebook['id']?></td>
            <td>
                <?php 
                if(isset($onebook['title']) && !empty($onebook['title'])){
                    echo $onebook['title'];
                }else{
                    echo "Not Available";
                }
                ?>
            </td>
            <td><?php echo $onebook['created']?></td>
            <td>
                <a href="edit.php?id=<?php echo $onebook['id'];?>">Edit</a> | 
                <a href="delete.php?id=<?php echo $onebook['id'];?>">Delete</a> 
            </td>
        </tr>
    </table>
</html>
