-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2016 at 01:04 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_project`
--
CREATE DATABASE IF NOT EXISTS `crud_project` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crud_project`;

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `birthday`, `created`, `updated`, `deleted`) VALUES
(1, '2016-03-02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-22'),
(2, '2016-03-01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(3, '2016-03-01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(4, '1994-11-25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(5, '1995-04-11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(6, '1995-05-12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00'),
(7, '1999-02-01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(15) NOT NULL,
  `title` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'Database Programming ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Object Oriented Programming', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-22'),
(7, 'C++', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'OOP', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(15) NOT NULL,
  `hobby` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobby`, `created`, `updated`, `deleted`) VALUES
(1, 'Cricket , Codding', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Cricket,Football,Codding,Gardening', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby2`
--

CREATE TABLE `hobby2` (
  `id` int(15) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby2`
--

INSERT INTO `hobby2` (`id`, `hobby`, `created`, `updated`, `deleted`) VALUES
(1, 'Cricket', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE `mobiles` (
  `id` int(15) NOT NULL,
  `title` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'Nokia 1200', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-24'),
(2, 'Nokia 2690', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Walton Primo F4', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-24'),
(4, 'Walton Primo E6', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-24'),
(5, 'Symphoni x123', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 'Web Development', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-25');

-- --------------------------------------------------------

--
-- Table structure for table `textarea`
--

CREATE TABLE `textarea` (
  `id` int(15) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `textarea`
--

INSERT INTO `textarea` (`id`, `title`, `created`, `updated`, `deleted`) VALUES
(1, 'Aspire Tech Services & Solutions', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-25'),
(2, 'Bangladesh University.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-25'),
(3, 'BASIS INSTITUTE OF TECHNOLOGY & MANAGEMENT', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'Kalikapur High School', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby2`
--
ALTER TABLE `hobby2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `textarea`
--
ALTER TABLE `textarea`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hobby2`
--
ALTER TABLE `hobby2`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `textarea`
--
ALTER TABLE `textarea`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
